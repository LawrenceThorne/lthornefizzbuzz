# FizzBuzz TDD (Test Driven Development) with documentation
- AKA HelloWorld on steriods
- Lawrence Thorne (Lawrence.Thorne@gmail.com)

## Application Requirements
 - The program has 1 current business requirement a write Hello World to the console/screen.
	- NOTE: Modified to print out FizzBuzz test instead because a simple Hello World is boring.
		- Prints out numbers from 1 to 100
		- If divisible by 3 replace output with "Fizz"
		- If divisible by 5 replace output with "Buzz"
		- If divisible by 3 AND 5 replace output with "FizzBuzz"
- The program should have an API that is separated from the program logic to eventually support mobile applications, web applications, console applications or windows services.
- The program should support future enhancements for writing to a database, console application, etc.
- Use common design patterns (inheritance, e.g.) to account for these future concerns.
- Use configuration files or another industry standard mechanism for determining where to write the information to. 
- Write unit tests to support the API.

## Solution Projects
- LThorneFizzBuzz
	- Base console app
		- Build to run console app
- LThorneFizzBuzz.API
	- RESTful API
		- Build to run web based API on preferred port (default StartUp Project)
- LthorneFuzzBuzzLibrary.tests
	- Unit Tests
		- NOTE: In Visual Studio Test Explorer > Run All Tests (if you have VS2015 ENT, tests are run automatically on build)
		- All Unit Tests defined in LthorneFuzzBuzzerTests.cs
- LThorneFizzBuzzLibrary
	- Shared application Methods/Library
		
### API
- Api endpoint can be found at /api/values (or linked from Homepage in browser) 

### Application Setup
	- Nuget packages
		- NUnit - 3.0.0
			- (NUnit v 3.11 + NUnit Test Adapter v 3.11 throws Meta data errors)
		- NUnit Test Ddapter v 3.11
			- Tested and working with NUnit 3.0.0
			- Allows Unit Tests on local dev environment in Visual studio (Test Explorer)
			- Suited for CI as well

### Enable the Test Explorer in Visual Studio 2015
- Top Nav > Test > Windows > Test Explorer.

## Custom Config
- LThorneFuzzBuzz.API
	- WebApiConfig.cs
		- Configured to return JSON instead of XML

## Adding Db functionality
- add a data connection to your Web.Config inside the <connectionStrings> node, replacing your local SQL credentials with the sample below:
	- connectionString="Server=myServerAddress;Database=myDataBase;User Id=myUsername;Password=myPassword;" />

## Enjoy! and have a nice day.

	
	
	
	
	
