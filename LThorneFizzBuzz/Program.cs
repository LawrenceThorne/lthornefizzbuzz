﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LThorneFizzBuzz;
using LThorneFizzBuzzLibrary;

namespace LThorneFizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            
            for (int i = 1; i <= 100; i++)
            {
                Console.WriteLine(LThorneFizzBuzzer.GetValue(i));
            }

            Console.ReadLine();
        }

    }
}
