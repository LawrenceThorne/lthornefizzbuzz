﻿using LThorneFizzBuzzLibrary;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LThorneFizzBuzz.Library.Tests
{

    [TestFixture]
    public class LThorneFizzBuzzerTests
    {

        /*
            Default test method to make sure our Nuget packages 
            for NUnit and NUnit Adapter to make sure 
            the package versions play well together.
        */
        [Test]
        public void SampleTest()
        {
            Assert.Pass();
        }


        //to be deprecated once dev cycles begin
        /* 
            test test - verifies proper framework - remove once actually dev'ing.
        */

        /*
            [Test]
            public void LThorneBuzzer_When1_Returns1()
            {
                // Arrange
                int inputVal = 1;

                // Act
                string output = LThorneFizzBuzzer.GetValue(inputVal);

                // Assert
                Assert.AreEqual("1", output);

            }
        */



        /* 
            default test
        */
        [Test]
        public void LThorneBuzzer_WhenDefault_ReturnsInput(
                [Values(1, 2, 4, 7, 8, 11, 13, 14, 16, 17, 19)] int inputVal
            )
        {
            string output = LThorneFizzBuzzer.GetValue(inputVal);
            Assert.AreEqual(inputVal.ToString(), output);

        }




        /* 
            3 test
        */
        [Test]
        public void LThorneBuzzer_WhenDiv3_ReturnsFizz(
            [Values(3, 6, 9, 12, 18)] int inputVal
            )
        {

            //int inputVal = 3;
            string output = LThorneFizzBuzzer.GetValue(inputVal);
            Assert.AreEqual("Fizz", output);

        }





        /* 
            5 test
        */
        [Test]
        public void LThorneBuzzer_WhenDiv5_ReturnsFizz(
            [Values(5, 10, 20)] int inputVal
            )
        {

            //int inputVal = 5;
            string output = LThorneFizzBuzzer.GetValue(inputVal);
            Assert.AreEqual("Buzz", output);

        }




        /* 
            Both test
        */
        [Test]
        public void LThorneBuzzer_WhenDivBoth_ReturnsFizzBuzz(
            [Values(15)] int inputVal 
            )
        {

            //int inputVal = 5;
            string output = LThorneFizzBuzzer.GetValue(inputVal);
            Assert.AreEqual("FizzBuzz", output);

        }


    }
}
