﻿using LThorneFizzBuzzLibrary;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LThorneFizzBuzz.API.Controllers
{
    //[Authorize] // add for API auth later 
    // for noobs only used to ViewJS, RequireJS, Angular, et al: dont put your API key in the front end code!
    //- use server side tokens pulled from CMS/DB or put them into you web.config. 
    //Google it if you don't know how.
    // http://bfy.tw/Km5g

    public class ValuesController : ApiController
    {
        // GET api/values
        //returns VERY simple Dictionary object as json
        public Dictionary<string, string> Get()
        {

            Dictionary<string, string> returnVal = new Dictionary<string, string>();

            try {

                returnVal.Add( "status", "success" );
                

                for (int i = 1; i <= 100; i++)
                {
                    //Console.WriteLine(LThorneFizzBuzzer.GetValue(i));
                    returnVal.Add("item"+ i, LThorneFizzBuzzer.GetValue(i));
                }

            }
            catch(InvalidCastException e)
            {
                returnVal.Add( "status", "fail" );
                returnVal.Add( "details", e.ToString() );

            }

            return returnVal;
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
